# Create Your First Index
Time: 20 minutes

## Instructions
The purpose of this lab is to create an index manually. After that, you'll link to data in a local file that will be indexed.


---

### Create an Index
Let's create your first index using the Splunk UI. Try not to simply follow these steps; take a moment to review each field in the form, and ask questions if something is not clear.

- Go to `Settings`
- In the data section, click on `Indexes`
- Click on the `New Index` button
  - Set the name as `syslog`
  - Change the max size of entire index to `100` and choose the `MB` option
  - Choose the `Search & Reporting` app
  - Click on "Save"

---

### Link to Local File as Data Input
Let's add data into the new index. You'd normally forward data from a remote source, but there are times when you might need to comprehend how Splunk understands a file or what you can you do with the data indexed.

- Go to `Settings`
- Under the Data section, click on `Data inputs`
- On the Data inputs page, click on `Files & Directories`
- On the next page, click on the `New Local File & Directory` button
  - Browse to Locate the `/var/log/syslog` file.
  - Check the `Continuously Monitor` option
  - Click on `Next` (under the nav menu) to advance to the set source type screen
  - You will notice that in the `Source type` Splunk should Identify it as a `syslog`, We will use that.  If need be you can select a different sources type or even create a new one.
  - Click on `Next` to proceed
  - In the input settings window, ensure that the app context is `Search & Reporting` and set the index for this data input to the `syslog` index
  - Click on the `Review` button
  - Click on 'Submit'
- Click on the `Start Searching` button

The search that Splunk takes you to sould look something like this:

```
source="/var/log/syslog" host="splunk" index="syslog" sourcetype="syslog"
```

Explore the new events that are now indexed in splunk.
